// Interact with the length and capacity of an array

package main

import "fmt"

func main()  { // Make creates a Slice with an arrangement in which you can define length and capacity
	slice := make([]int,3,5) // int = Type of slice , 3 = Slice length, 5 = Slice capacity
	fmt.Println(slice)
	fmt.Println(len(slice)) // This allows us to see the length of the array
	fmt.Println(cap(slice)) // This allows us to see the capacity of the array

	slice = append(slice, 2) // Append adds a value within the array regardless of its capacity
	fmt.Println(slice)
}