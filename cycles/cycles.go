// Cycle for and different ways to use it

package main

import "fmt"

func main()  {
	fmt.Printf("------Cycle For------\n")
	for a:=0;a<11;a++ { // This would be the standard for the cycle for
		fmt.Printf("%d\n",a)
	}
	fakeWhile()
}

func fakeWhile() {
	b:=0
	fmt.Printf("------Fake While------\n")
	for { // In Go it is possible to use the for cycle in this way and thus simulate a while cycle
		fmt.Printf("%d\n",b)
		b++ // In this way the cycle would be infinite

		if b>10 { // To end the cycle we will use a break
			break
		}
	}
}