// How to initialize a web server, first steps to create an API

package main

import (
	"fmt"
	"io"
	"net/http" // Library used for web
)

func main() {

	http.HandleFunc("/", handler) // after a diagonal we will respond with a function called "handler"

	http.HandleFunc("/hello", func(w http.ResponseWriter, r *http.Request) { // We can create the function directly after the method
		io.WriteString(w, "Wow, this is awesome!")
		fmt.Println("There is a new request") // every time we make a request this msg will appear
	})

	http.ListenAndServe(":8000", nil) // We declare on which port the server is going to run

}

func handler(w http.ResponseWriter, r *http.Request) {
	// "ResponseWriter" is a structure that allows us to define how we will respond to the request
	// "Request" is a pointer to the request information

	io.WriteString(w, "hello world") // "w" -> is an object that represents a string of write data (writable String)
}

/*
	For the server to start the program must be run,
	this will remain active during server execution waiting for requests.
	to close the server we use the "Ctrl + C" in terminal
*/
