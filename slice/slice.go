// Slice structure

package main

import "fmt"

func main()  {
	array := [4] string {"a","b","c","d"}
	slice := array[1:3] // We will take the value between position "0" and position "1"
	fmt.Println(slice)
}