// Methods for structures

package main

import "fmt"

type User struct{
	age int
	name, surname string
}

func (user User) fullName() string{
	return user.name + " " + user.surname
}

func (user *User) setName(n string){ // If we do not use a pointer, the name will not be affected because it would copy the value instead of its address in memory
	user.name = n
}

func main()  {
	uriel := new(User)

	uriel.name = "uriel"

	uriel.setName("Marcos")

	fmt.Println(uriel.name)
}