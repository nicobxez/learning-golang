// How to create and use structures

package main

import "fmt"

/*
	1. Type declares that there will be a new type
	2. User would be the name of that type
	3. Finally we define that this is going to be a structure
*/

type User struct{ // User would be like a data set
	age int
	name, surname string
}

func main()  {
	uriel := User{age: 24, name: "Uriel", surname: "Hernandez"} // We are saying that "uriel" will have that set of data with these values

	fmt.Println(uriel.surname)
}