// Panic is a controlled error that returns and pauses all functions that continue from this "enter in panic"
// Recover is used to stop a panic

package main

import (
	"fmt"
	"io/ioutil"
)

func main() {
	executeReadFile()
	fmt.Println("this text will never be seen")
}

func executeReadFile() {
	execution := readFile()
	fmt.Println(execution)
}

func readFile() bool {
	fileData, err := ioutil.ReadFile("./hello.txt") // This address is wrong on purpose to return an error

	defer func ()  { // If you comment or delete this function, all other functions would panic
		r := recover()
		fmt.Println(r)
	}()

	if err != nil {
		panic(err)
	}

	fmt.Println(string(fileData))

	return true
}