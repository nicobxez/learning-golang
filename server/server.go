// How to serve files through the server

package main

import (
	"net/http"
)

func main() {
	fileServer := http.FileServer(http.Dir("server/public")) // with this we will say that you use only the files that are inside public
	http.Handle("/", http.StripPrefix("/", fileServer))

	http.ListenAndServe(":8000", nil)
}

/*
	To avoid exposing our code or confidential data, we can use a public folder,
	otherwise through a HandleFunc we can use a path
	http.ServeFile(w,r,r.URL.Path[1:]) or specify the route ex:"server/index.html"
*/
