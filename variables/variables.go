// Ways to declare a variable

package main // Standard package

import "fmt" // This library will allow us to read and print data

func main() {
	fmt.Printf("\n---- Variables ----\n")

	var num int 				// The type of variable is declared at the end
	num = 25
	fmt.Println(num)
	num = 40
	fmt.Println(num)

	name := "Alejandro"			// The colon is for you to detect the type of variable automatically
	name = "Pedro"
	fmt.Println(name)

	name, num = "Rafael", 32 	// We declare different variables at the same time
	fmt.Println(name, num)
 
	name2 := "Ramon"
	name, name2 = name2, name 	// Exchange values between variables
	fmt.Println(name, name2)

	multi := 25
	fmt.Println(num * multi)   // Multiply 32 x 25

	main2()
}

// Global Variable
var race = "Sj" 	// Outside of a function it is mandatory to use "var"

// Functions
func main2() {
	fmt.Printf("\n---- Variables 2 ----\n")

	num := 25
	name := "Alejandro"
	num2 := 54
	userName := "admin"
	Num := 46 				   					// It is case sensitive, so "Num" and "num" are two different variables

	fmt.Println(num, name, num2, userName, Num)

	var ( 					  					// Declaration of multiple variables, in this way it also automatically detects the type of variable
		dios = "Goku"
		enemy1, enemy2 = "Babidi", "Cell"
	)

	var num3 = 66 								// Not exactly with the two points we activate the autoType, if we do not declare the type, Go it is responsible for deciphering which one it is

	fmt.Println(dios, enemy1, enemy2, num3)

	print()
}

func print() {
	fmt.Println("Goku's race is: " + race)
}

// It is not necessary to add the ";" since the compiler detects and adds it automatically