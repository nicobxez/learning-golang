// It allows us to replicate the inheritance behavior of object-oriented programming

package main

import "fmt"

// We create two structure

type Human struct {
	name string
}

type Dummy struct {
	name string
}

// We create a structure that contains the data of the previous two

type Tutor struct {
	Human
	Dummy
}

// We create a function to obtain data from different structures and interact with them

func (this Human) talk() string {
	return "What's up, "
}

func (this Tutor) talk() string {
	return this.Human.talk() + "Welcome to Golang"
}

func main() {
	tutor := Tutor{Human{"Uriel"}, Dummy{"Nico"}} // We declare a variable that will contain the data of the two structures

	fmt.Println(tutor.Human.name) // We enter the variable, the structure and your object
	fmt.Println(tutor.Dummy.name)
	fmt.Println(tutor.talk())
}
