// Type conversion ex: from int to string

package main

import (
	"fmt"
	"strconv" // With this library we can convert values to different types
)

func main() {
	age := 22
	age2 := "22"

	ageStr := strconv.Itoa(age) // Itoa converts an integer to a string
	
	ageInt, _ := strconv.Atoi(age2) // Atoi converts a string to an integer
	// The underscore is a variable that takes a value and ignores it, it is used when we want to obtain only one of multiple values

	fmt.Println("My age is " + ageStr)

	fmt.Println(ageInt + 18)
}
