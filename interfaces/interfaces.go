// A structure that defines empty methods

package main

import "fmt"

type User interface{ // An interface is a structure that defines empty methods and in turn is a type of data
	Permissions() int // 1-5
	Name() string
}

type Admin struct{ // We create a structure
	name string
}

type Editor struct{
	name string
}

func (this Admin) Permissions() int{ // To this structure we concatenate Permissions() of the interface
	return 5
}

func (this Admin) Name() string{
	return this.name
}

func (this Editor) Permissions() int{
	return 3
}

func (this Editor) Name() string{
	return this.name
}

func auth(user User) string{ // Function that will simulate the authentication, we pass the entire interface
	if user.Permissions() > 4 {
		return user.Name() + " have administrator permissions"
	} else if user.Permissions() == 3 {
		return user.Name() + " have editor permissions"
	}
	return ""
}

func main()  {
	users := []User{Admin{"Uriel"},Editor{"Fulanin"}} // We store the structure that implements an interface within an array of that interface
	
	for _,user := range users{
		fmt.Println(auth(user))
	}
}