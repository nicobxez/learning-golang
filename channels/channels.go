// The Gochannels allow us to communicate Gortuines between them

package main

import "fmt"

func main() {
	channel := make(chan string) // "chan" is the name assigned to declare a channel

	go func(channel chan string) { // assign an identifier and declare that the function will receive a string channel
		for {
			var name string
			fmt.Scanln(&name)
			channel <- name // when we receive a data "channel" goes from the left side
		}
	}(channel)

	fmt.Println("Enter a message:")

	msg := <-channel // when we send a data "channel" goes from the right side

	fmt.Println("I am printing what I received from the channel " + msg)
}

/*
	The Goroutine will not stop requesting information because it is in an infinite cycle
	The way to end the cycle is through waiting for channel information
	with "msg" we request a data, then the Goroutine waits until that data is sent to continue
	get the data, continue the execution of the code and print it
*/