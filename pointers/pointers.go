// Use data memory with pointers

package main

import "fmt"

func main()  {
	/*
		1. It is a memory address
		2. Instead of value, we have the address where it is stored
		3. X, Y => as123d => 3
		4. X => as123d => 6
		5. Y ¿? => 6
		6. We modify the address in memory of X but since Y points to the same address it is also affected
	*/
	var x,y *int
	integer := 3

	x = &integer
	y = &integer

	*x = 6

	fmt.Println(*x)
	fmt.Println(*y)
}

/*
	To access the memory address of a variable we will use "&"
	To access the value stored in a memory address we use the "*"
*/