// Print and read in the terminal

package main

import "fmt"

func main() {
	var age int
// "Printf" is an output, by replacing "f" with "ln" we will make a line break at the end
// "Scanf" is an input, inside the quotes we specify the verb and then the name of the variable
	fmt.Println("Enter your age:")
	fmt.Scanf("%d", &age)
	fmt.Printf("Your age is %d.", age)
}

/* 
"%v" use the default value of the variable (this is used in case of not knowing what type the data will arrive)
"%s" says the value is going to be of type string
"%d" says the value is going to be of type integer
"%f" says the value is going to be of type float
"%g" says the value is going to be of type decimal
"%g" says the value is going to be of type decimal in scientific notation
"%t" says the value is going to be of type boolean
*/