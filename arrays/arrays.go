// Use arrays in Go

package main

import "fmt"

func main() {
	var arreglo [10]int         // name, size and type of value
	arreglo2 := [3]int{1, 2, 3} // Between braces we can declare the values
	fmt.Println(arreglo)
	fmt.Println(arreglo2)

	var matriz [3][2]int // First value is the number of arrangements, second value is the number of elements within each arrangement
	matriz[0][1] = 1     // To position ourselves we declare array number and item number
	fmt.Println(matriz)
}
