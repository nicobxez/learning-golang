// How to create your own packages in Go and use them locally

package main

import (
	"fmt"
	"./dummy" // the route must be added, in this case it is "./"
)

func main() {
	fmt.Println(dummy.HelloWorld())
}
