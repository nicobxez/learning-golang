package dummy // package name

var hello_world string

func init() { // function used to preconfigure
	hello_world = "hello!"
}

func HelloWorld() string {
	return hello_world
}
