// The different conditionals

package main

import "fmt"

func main()  {
	x := 10
	y := 5

	if (x > 5) {
		fmt.Printf("%d is greater than %d\n", x, y)
	} else {
		fmt.Printf("I have entered the else")
	}
}

/*
	== Equal to (compare 2 values BUT you do it without checking the type of variables)
	=== Very equal to (compare the content of the variable and that the two variables are of the same type)
	!= Different to
	< Smaller than
	> Greater than
	<= Less than or equal to
	>= Greater than or equal to
	&& AND
	|| OR
*/