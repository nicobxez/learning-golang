// How to read or open a file

package main

import (
	"bufio"
	"fmt"
	"io/ioutil" // This library is the one that will allow us to read the file
	"os"        // Library that allows you to open files
)

func main() {
	execution := openFile()
	fmt.Println(execution)

}

func openFile() bool {
	fileData, err := ioutil.ReadFile("read_file/hello.txt") // The address will depend on where the binary is running
	archive, err := os.Open("read_file/hello.txt")

	defer func() { // Defer is used to execute a method or function regardless of when the current function ends
		archive.Close()
		fmt.Println("Defer")
	}( /* Close the file we have opened, for example, if the file is from a USB memory, with this method we notify the system that the file was closed */ )

	if err != nil {
		fmt.Println("Oops, there was an error")
	}

	scanner := bufio.NewScanner(archive)

	var i int

	fmt.Println("-----------")
	fmt.Println(string(fileData))
	fmt.Println("-----------")

	for scanner.Scan() { // This will allow us to read the file line by line
		i++
		line := scanner.Text()
		fmt.Println(i, line) // we will use a value to identify the line number
	}

	fmt.Println("-----------")

	if true { // It's a trap!
		return true
	}

	fmt.Println("this text will never be seen")

	return true
}
