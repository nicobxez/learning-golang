// Goroutine changes synchronous program to concurrent

package main

import (
	"fmt"
	"strings"
	"time"
)

func main() {
	go mySlowName("uriel") // "go" is the name assigned to declare a Goroutine
	fmt.Println("How boring")
	var wait string
	fmt.Scanln(&wait) // We improvise a kind of pause so that the program does not end early
}

func mySlowName(name string) {
	letters := strings.Split(name, "") // With Split we can separate the letters of a string

	for _, letter := range letters {
		time.Sleep(1000 * time.Millisecond) // Every time you enter an iteration wait 1 sec
		fmt.Println(letter)
	}
}
