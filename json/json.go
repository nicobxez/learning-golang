// How to use a go library to render a JSON

package main

import (
	"encoding/json"
	"net/http"
)

type Course struct { // The first letter of the attributes must be in uppercase, but this can be altered...
	Title     string `json:"title"`
	NumVideos int    `json:"video_number"`
}

type Courses []Course

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		course := Courses{
			Course{"Hello World", 3},
			Course{"I see you...", 2},
			Course{"I am behind you", 1}, // Unlike all other languages, in Go the last element must have a comma ","
		}

		json.NewEncoder(w).Encode(course)
	})

	http.ListenAndServe(":8000", nil)
}
