// How to copy arrays

package main

import "fmt"

func main()  {
	slice := []int{1,2,3,4} // we create an array with 4 values
	empty := make([]int,len(slice)) // we create an empty array with the same length as the slice array
	
	copy(empty,slice) // we copy the values of the "slice" array and pass them to the "copy" array

	fmt.Println(slice)
	fmt.Println(empty)
}

/*
	Data to consider:
		- Copy the minimum of items in both arrangements
		- we can increase the capacity of an array using a multiplier ex. "cap (slice) * 2"
*/